#include <Servo.h> //βιβλιοθικη του servo

#include <Arduino.h>
#include <Wire.h>        // Instantiate the Wire library

//Servo Variables
Servo servoMtr;

//Sensor variables
 const int trigger=1; 
const int echo=0; 

float duration;
float distance;



// Motor Drive control pins
const int LeftMotorDIR = 13;
const int RightMotorDIR = 12;
const int LeftMotorPwr = 11;
const int RightMotorPWR = 10;

// Sensor Inputs

const int recieveSensor = 0;
const int transmitSensor = 1;

//Servo Signal
const int servoPinSig = 3;


void setup() {
  motorInitialization();
  servoInitialization();
  sensorInitialize();
  Serial.begin(9600);
}

void loop()
{
  main_functionality();
}



void servoInitialization() ////αρχικοποιεί τα pins του servo
{
  pinMode(servoPinSig, OUTPUT);
  servoMtr.attach(3);
servoMtr.write(0);
}

void sensorInitialize()//αρχικοποιεί τα pins του αισθητηρα
{
  pinMode(trigger,OUTPUT);
  pinMode(echo,INPUT);

}

void motorInitialization() //αρχικοποιεί τα pins των κινητηρων
{
  pinMode(RightMotorPWR, OUTPUT);
  pinMode(LeftMotorPwr, OUTPUT);
  pinMode(RightMotorDIR, OUTPUT);
  pinMode(LeftMotorDIR, OUTPUT);
}


void turnRight() // καθοριζει την κατευθηνση αντιστρεφοντας τους δεξιους κινητηρες και δινωντας περισοτερη ισχυ στον αριστερα
{ digitalWrite(LeftMotorDIR, HIGH);
  digitalWrite(RightMotorDIR, HIGH);

  analogWrite(LeftMotorPwr, 255);
  analogWrite(RightMotorPWR, 120 );

  Serial.println("deksia");

}

void turnLeft()// καθοριζει την κατευθηνση αντιστρεφοντας τους αριστερους κινητηρες και δινωντας περισοτερη ισχυ στον δεξιους
{ digitalWrite(LeftMotorDIR, LOW);
  digitalWrite(RightMotorDIR, LOW);

  analogWrite(LeftMotorPwr, 120);
  analogWrite(RightMotorPWR, 255);
    Serial.println("aristera");

}
void moveForward()// καθοριζει την κατευθηνση  στους αριστερους κινητηρες
{ digitalWrite(LeftMotorDIR, LOW);
  digitalWrite(RightMotorDIR, HIGH);

  analogWrite(LeftMotorPwr, 255);
  analogWrite(RightMotorPWR, 255);
    Serial.println("mpros");

}

void moveBackwards()// καθοριζει την κατευθηνση αντιστρεφοντας τους  κινητηρες
{ digitalWrite(LeftMotorDIR, HIGH);
  digitalWrite(RightMotorDIR, LOW);

  analogWrite(LeftMotorPwr, 255);
  analogWrite(RightMotorPWR, 255);
    Serial.println("piso");

}

void stopMovement()// καθοριζει την κατευθηνση και σταματαει την τροφοδοσια στους κινητηρες
{
  digitalWrite(LeftMotorDIR, HIGH);
  digitalWrite(RightMotorDIR, LOW);

  analogWrite(LeftMotorPwr, 0);
  analogWrite(RightMotorPWR, 0);
    Serial.println("stamatima");

}

float lookRight() { //γυρναει το σερβο στης 50 μοιρες  και διαβαζει την δεξια αποσταση και το επαναφερει στο κεντρο
     Serial.println("koita deksia");

  servoMtr.write(0);
  delay(500);
 getDistance();
  delay(100);
  servoMtr.write(45);
  return distance;
}

float lookLeft() { //γυρναει το σερβο στης 170 μοιρες διαβαζει την τιμη της αριστερας αποστασης και το επαναφερει στο κεντρο
      Serial.println("koita aristera");

  servoMtr.write(90);
  delay(500);
  getDistance();
  delay(100);
  servoMtr.write(45);
  return distance;
  delay(100);
}
void getDistance()
{
float tempDist=0.0;
  digitalWrite(trigger, LOW);
  delayMicroseconds(5);
  digitalWrite(trigger, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigger, LOW);
  duration = pulseIn(echo, HIGH);
  distance = duration * 0.034 / 2;
        Serial.println(distance);

}


void main_functionality(){
   float distanceRight = 0;
  float distanceLeft = 0;
  delay(50);
  getDistance();
delay(100);
  if (distance <= 20 ){
  stopMovement();
    delay(300);
    moveBackwards();
    delay(400);
    stopMovement();
    delay(300);
    distanceRight = lookRight();
    delay(300);
    distanceLeft = lookLeft();
    delay(300);

    if (distance >= distanceLeft){
      turnRight();
      stopMovement();
    }
    else{
      turnLeft();
      stopMovement();
    }
  }
  else{
    moveForward(); 
  }
   
  }
